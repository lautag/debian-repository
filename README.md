# Updating Repo

In `public/debian` in a docker container with `apt-utils` installed:

```shell
apt-ftparchive packages . > Packages
gzip -k -f Packages
apt-ftparchive release . > Release
```

On a machine where the signing key is installed in `public/debian`

```shell
gpg -u "<email>" -abs -o - Release > Release.gpg
gpg -u "<email>" --clearsign -o - Release > InRelease
```

# Usage

```shell
curl -s --compressed "https://lautag.gitlab.io/debian-repository/debian/KEY.gpg" | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/laut.gpg >/dev/null
sudo curl -s --compressed -o /etc/apt/sources.list.d/my_list_file.list "https://lautag.gitlab.io/debian-repository/debian/laut.list"
apt update
```